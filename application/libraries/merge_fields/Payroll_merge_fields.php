<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll_merge_fields extends App_merge_fields
{
    public function build()
    {
        return [
                [
                    'name'      => 'Staff/Contact who take action on payroll',
                    'key'       => '{payroll_user_take_action}',
                    'available' => [
                        'payrolls',
                    ],
                ],
                [
                    'name'      => 'Payroll Link',
                    'key'       => '{payroll_link}',
                    'available' => [
                        'payrolls',
                    ],
                ],
                [
                    'name'      => 'Payroll Number',
                    'key'       => '{payroll_number}',
                    'available' => [
                        'payrolls',
                    ],
                ],
                [
                    'name'      => 'Payroll Remarks',
                    'key'       => '{payroll_remarks}',
                    'available' => [
                        'payrolls',
                    ],
                ],
                [
                    'name'      => 'Payroll Priority',
                    'key'       => '{payroll_priority}',
                    'available' => [
                        'payrolls',
                    ],
                ],
                [
                    'name'      => 'Payroll Start Date',
                    'key'       => '{payroll_startdate}',
                    'available' => [
                        'payrolls',
                    ],
                ],
                [
                    'name'      => 'Payroll Due Date',
                    'key'       => '{payroll_duedate}',
                    'available' => [
                        'payrolls',
                    ],
                ],
            ];
    }

    /**
     * Merge fields for payrolls
     * @param  mixed  $payroll_id         payroll id
     * @param  boolean $client_template is client template or staff template
     * @return array
     */
    public function format($payroll_id, $client_template = false)
    {
        $fields = [];

        $this->ci->db->where('id', $payroll_id);
        $payroll = $this->ci->db->get(db_prefix().'payrolls')->row();

        if (!$payroll) {
            return $fields;
        }

        // Client templateonly passed when sending to payrolls related to project and sending email template to contacts
        // Passed from payrolls_model  _send_payroll_responsible_users_notification function
        if ($client_template == false) {
            $fields['{payroll_link}'] = admin_url('payroll/viewpay/' . $payroll_id);
        } 

        if (is_client_logged_in()) {
            $fields['{payroll_user_take_action}'] = get_contact_full_name(get_contact_user_id());
        } else {
            $fields['{payroll_user_take_action}'] = get_staff_full_name(get_staff_user_id());
        }

        $fields['{payroll_number}'] = $payroll->payroll_number;
        $fields['{payroll_remarks}'] = $payroll->remarks;

        $languageChanged = false;

        // The payrolls status may not be translated if the client language is not loaded
        if (!is_client_logged_in()
        && $payroll->rel_type == 'project'
        && $project
        && isset($GLOBALS['SENDING_EMAIL_TEMPLATE_CLASS'])
        && !$GLOBALS['SENDING_EMAIL_TEMPLATE_CLASS']->get_staff_id()) 
        {
            load_client_language($project->clientid);
            $languageChanged = true;
        } else {
            if (isset($GLOBALS['SENDING_EMAIL_TEMPLATE_CLASS'])) {
                $sending_to_staff_id = $GLOBALS['SENDING_EMAIL_TEMPLATE_CLASS']->get_staff_id();
                if ($sending_to_staff_id) {
                    load_admin_language($sending_to_staff_id);
                    $languageChanged = true;
                }
            }
        }

        $fields['{payroll_priority}'] = $payroll->priority;

        $custom_fields = get_custom_fields('payrolls');
        foreach ($custom_fields as $field) {
            $fields['{' . $field['slug'] . '}'] = get_custom_field_value($payroll_id, $field['id'], 'payrolls');
        }

        if (!is_client_logged_in() && $languageChanged) {
            load_admin_language();
        } elseif (is_client_logged_in() && $languageChanged) {
            load_client_language();
        }

        $fields['{payroll_startdate}'] = _d($payroll->startdate);
        $fields['{payroll_duedate}']   = _d($payroll->duedate);

        return hooks()->apply_filters('payroll_merge_fields', $fields, [
        'id'              => $payroll_id,
        'payroll'         => $payroll,
        'client_template' => $client_template,
     ]);
    }
}
