<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll_deadline_reminder_to_staff extends App_mail_template
{
    protected $for = 'staff';

    protected $staff_email;

    protected $staffid;

    protected $payroll_id;

    public $slug = 'payroll-deadline-notification';

    public $rel_type = 'payroll';

    public function __construct($staff_email, $staffid, $payroll_id)
    {
        parent::__construct();

        $this->staff_email = $staff_email;
        $this->staffid     = $staffid;
        $this->payroll_id     = $payroll_id;
    }

    public function build()
    {
        $this->to($this->staff_email)
        ->set_rel_id($this->payroll_id)
        ->set_staff_id($this->staffid)
        ->set_merge_fields('staff_merge_fields', $this->staffid)
        ->set_merge_fields('payroll_merge_fields', $this->payroll_id);
    }
}
