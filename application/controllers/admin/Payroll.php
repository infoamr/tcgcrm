<?php

use app\services\utilities\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payroll_model');
        $this->load->model('Clients_model');
        $this->load->library('form_validation');
    }

    /* List all clients */
    public function index()
    {
        if (!has_permission('payrolls', '', 'view')) {
            if (!has_permission('payrolls', '', 'create')) {
                access_denied('payrolls');
            }
        }

        $data['fetch_data'] = $this->payroll_model->get_all_payroll();
        $data['states_data'] = $this->payroll_model->get_states();
        $this->load->view('admin/payrolls/manage', $data);
    }

    public function paypdf()
    {
        if (!has_permission('payrolls', '', 'view')) {
            if (!has_permission('payrolls', '', 'create')) {
                access_denied('payrolls');
            }
        }

        $this->load->library('pdf');
        $data['fetch_data'] = $this->payroll_model->get_all_payroll();
        $html = $this->load->view('admin/payrolls/pdfpayroll', $data, true);
        // $this->pdf->createPDF($html, 'payrollpdf', true);

        $dompdf = new Dompdf\Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $pdf = $dompdf->output();
        $dompdf->stream('PayrollList.pdf');
    }

    /* Add new payroll */
    public function pay($id = '')
    {
        if (!has_permission('payrolls', '', 'create')) {
            access_denied('payrolls');
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('client', 'Company', 'required');
            $this->form_validation->set_rules('startdate', 'Start Date', 'required');
            $this->form_validation->set_rules('duedate', 'Due Date', 'required');
            $this->form_validation->set_rules('federal_form', 'Federal Form', 'required');
            $this->form_validation->set_rules('federal_status', 'Federal Status', 'required');
            $this->form_validation->set_rules('assigned_to', 'Assigned To', 'required');

            if ($this->form_validation->run() !== false) {
                $data = $this->input->post(); 
                // echo '<pre>'; print_r($data); echo '</pre>';exit;
                $data['startdate'] = date('Y-m-d',strtotime($data['startdate']));
                $data['duedate'] = date('Y-m-d',strtotime($data['duedate']));
                $data['payroll_number'] = 'PR'.date('is').rand(001,9999);

                $insert['payroll_number'] = $data['payroll_number'];
                $insert['federal_form'] = $data['federal_form'];
                $insert['federal_status'] = $data['federal_status'];
                $insert['client_id']  = $data['client'];

                $insert['priority'] = $data['priority'];
                $insert['assigned_to'] = $data['assigned_to'];
                $insert['startdate'] = $data['startdate'];
                $insert['duedate']  = $data['duedate'];

                // 1-year
                if(!empty($data['repeat_every']) && $data['repeat_every'] != 'custom')
                {
                    $rpev = explode("-",$data['repeat_every']);
                    $insert['recurring_type'] = $rpev[1]; // year
                    $insert['repeat_every'] = $rpev[0]; // 1
                    $insert['recurring'] = 1;
                }

                if($data['repeat_every'] == 'custom')
                {
                    $insert['custom_recurring'] = 1;
                    $insert['recurring'] = 1;
                }

                $insert['cycles'] = isset($data['cycles']) ? $data['cycles']: 0; // 0 if infinity
                //$insert['total_cycles'] = $data['cycles']; //  0 if infinity
                $insert['is_recurring_from'] = NULL; 
                $insert['last_recurring_date'] = NULL;
                $insert['remarks']    = $data['remarks'];
                $insert['dateadded']  = date('Y-m-d h:i:s');

                $this->db->insert(db_prefix() . 'payrolls', $insert);
                $insert_id = $this->db->insert_id();
                //  echo $insert_id; exit;
                log_activity('New Payroll Added [ID:'.$insert_id.', Number: '.$data['payroll_number'].']');

                if ($insert_id) 
                {
                    $stf_arr = array();
                    foreach ($this->session->userdata('stateform') as $sd) {
                       $stf_arr[] = $sd;
                    }
                    $com_ids = implode(",",$stf_arr);
                    $ids_arr = array($com_ids);
                    $states_insert['payroll_id'] = $insert_id;
                    $states_insert['state_forms_ids'] = json_encode($ids_arr);
                    $this->db->insert(db_prefix() . 'payroll_states', $states_insert);

                    if(!empty($data['reminder_date']) && !empty($data['staffemail']))
                    {
                        $rem_insert['payroll_id'] = $insert_id;
                        $rem_insert['description'] = $data['reminder_description'];
                        $rem_insert['email'] = $data['staffemail'];
                        $rem_insert['date'] = date('Y-m-d h:i:s', strtotime($data['reminder_date']));
                        $rem_insert['isnotified'] = 0;
                        $rem_insert['staff'] = get_staff_user_id();
                        $rem_insert['creator'] = get_staff_user_id();
                        // $rem_insert['notify_by_email'] = 1;
                        $this->db->insert(db_prefix() . 'payroll_reminders', $rem_insert);
                    }
                    
                    if(!empty($data['followers']))
                    {
                        foreach ($data['followers'] as $fld) {
                            $fol_insert['payroll_id'] = $insert_id;
                            $fol_insert['staffid'] = $fld;
                            $this->db->insert(db_prefix() . 'payroll_followers', $fol_insert);
                        }
                    }

                    set_alert('success', 'Payroll added successfully');
                    redirect(admin_url('payroll'));
                }
            }
        }

        $data['client_data'] = $this->Clients_model->get_all_customer();
        $data['client_type_data'] = $this->Clients_model->get_all_client_type();
        $data['staff_data'] = $this->Clients_model->get_all_staff();
        $data['staff_email'] = $this->clients_model->get_all_staff();
        $data['followers'] = $this->Clients_model->get_all_staff();
        $data['federal_forms'] = $this->payroll_model->get_federal_forms();
        $data['state_forms'] = $this->payroll_model->get_state_forms();

        $this->load->view('admin/payrolls/payroll', $data);
    }

    public function viewpay($id)
    {
        if (!has_permission('payrolls', '', 'view')) {
            access_denied('payrolls');
        }

        $data['followers'] = $this->payroll_model->get_followers_by_payrollid($id);
        $data['reminder'] = $this->payroll_model->get_reminder_by_payrollid($id);
        $data['states'] = $this->payroll_model->get_states_by_payrollid($id);
        $data['fetch_data'] = $this->payroll_model->get_payroll_byid($id);
        $this->load->view('admin/payrolls/view_payroll', $data);
    }

    public function paypopup($id)
    {
        if (!has_permission('payrolls', '', 'view')) {
            access_denied('payrolls');
        }

        // $data["followers"] = $this->payroll_model->get_followers_by_payrollid($id);
        // $data["reminder"] = $this->payroll_model->get_reminder_by_payrollid($id);
        // $data['fetch_data'] = $this->payroll_model->get_payroll_byid($id);
        // $this->load->view('admin/includes/modals/payroll', $data);
    }

    public function editpay($id)
    {
        if (!has_permission('payrolls', '', 'edit')) {
            access_denied('payrolls');
        }

        $data['fetch_data'] = $this->payroll_model->get_payroll_byid($id);
        $data['client_data'] = $this->Clients_model->get_all_customer();
        $data['client_type_data'] = $this->Clients_model->get_all_client_type();
        $data['staff_data'] = $this->Clients_model->get_all_staff();
        $data['followers'] = $this->Clients_model->get_all_staff();
        $data['payfollowers'] = $this->payroll_model->get_followers_by_payrollid($id);
        $data['reminder'] = $this->payroll_model->get_reminder_by_payrollid($id);
        $data['states'] = $this->payroll_model->get_states_by_payrollid($id);
        $data['staff_email'] = $this->clients_model->get_all_staff();
        $data['federal_forms'] = $this->payroll_model->get_federal_forms();
        $data['state_forms'] = $this->payroll_model->get_state_forms();

        if ($this->input->post()) 
        {
            $data = $this->input->post(); 
            // echo '<pre>'; print_r($data); echo '</pre>';exit;
            $request_array['client_id'] = $data['client'];
            $request_array['federal_form'] = $data['federal_form'];
            $request_array['federal_status'] = $data['federal_status'];
            $request_array['priority'] = $data['priority'];
            $request_array['assigned_to'] = $data['assigned_to'];
            $request_array['startdate'] = date("Y-m-d", strtotime($data['startdate']));
            $request_array['duedate'] = date("Y-m-d", strtotime($data['duedate']));
            $this->db->where('id',$id);
            $this->db->update(db_prefix() . 'payrolls',$request_array);

            if(!empty($data['states'])) {
                $stf_arr = array();
                foreach ($this->session->userdata('stateform') as $sd) {
                   $stf_arr[] = $sd;
                }
                $com_ids = implode(",",$stf_arr);
                $ids_arr = array($com_ids);
                $states_insert['payroll_id'] = $id;
                $states_insert['state_forms_ids'] = json_encode($ids_arr);
                $this->db->insert(db_prefix() . 'payroll_states', $states_insert);
            }

            if(!empty($data['reminder_date']))
            {
                $this->db->where('payroll_id', $id)->delete(db_prefix() . 'payroll_reminders');
                $rem_insert['payroll_id'] = $id;
                $rem_insert['description'] = $data['reminder_description'];
                $rem_insert['email'] = $data['staffemail'];
                $rem_insert['date'] = date('Y-m-d h:i:s', strtotime($data['reminder_date']));
                $rem_insert['isnotified'] = 0;
                $rem_insert['staff'] = get_staff_user_id();
                $rem_insert['creator'] = get_staff_user_id();
                // $rem_insert['notify_by_email'] = 1;
                // echo '<pre>'; print_r($rem_insert); echo '</pre>';exit;
                $this->db->insert(db_prefix() . 'payroll_reminders', $rem_insert);
            }

            if(!empty($data['followers']))
            {
                $this->db->where('payroll_id', $id)->delete(db_prefix() . 'payroll_followers');
                foreach ($data['followers'] as $fld) {
                    $fol_insert['payroll_id'] = $id;
                    $fol_insert['staffid'] = $fld;
                    $this->db->insert(db_prefix() . 'payroll_followers', $fol_insert);
                }
            }

            set_alert('success', 'Payroll updated successfully');
            redirect(admin_url('payroll'));
        }

        $this->load->view('admin/payrolls/edit_payroll', $data);
    }

    public function addcompany()
    {
        if ($this->input->post() && !$this->input->is_ajax_request()) 
        {
            if ($id == '') {
                if (!has_permission('payrolls', '', 'create')) {
                    access_denied('payrolls');
                }

                $data = $this->input->post();
                $save_and_add_contact = false;
                if (isset($data['save_and_add_contact'])) {
                    unset($data['save_and_add_contact']);
                    $save_and_add_contact = true;
                }
                $id = $this->clients_model->add($data);
                if (!has_permission('payrolls', '', 'view')) {
                    $assign['customer_admins']   = [];
                    $assign['customer_admins'][] = get_staff_user_id();
                    $this->clients_model->assign_admins($assign, $id);
                }
                if ($id) {
                    set_alert('success', _l('added_successfully', 'Company'));
                    redirect(admin_url('payroll/pay'));
                }
            }
        }
        //$this->load->view('admin/payrolls/payroll', $data);
    }

    /* Delete payroll from database */
    public function deletepay($id)
    {
        if (!has_permission('payrolls', '', 'delete')) {
            access_denied('payrolls');
        }

        $this->load->model('payroll_model');
        $success = $this->payroll_model->delete_payroll($id);
        
        $message = _l('problem_deleting', 'Payroll');
        if ($success) {
            $message = _l('deleted', 'Payroll');
            set_alert('success', $message);
        } else {
            set_alert('warning', $message);
        }
        redirect(admin_url('payroll'));
    }

    /* Delete payroll state from database */
    public function deletepaystate($id,$payid)
    {
        if (!has_permission('payrolls', '', 'delete')) {
            access_denied('payrolls');
        }

        $this->load->model('payroll_model');
        $success = $this->payroll_model->delete_paystate($id);
        
        $message = _l('problem_deleting', 'State');
        if ($success) {
            $message = _l('deleted', 'State');
            set_alert('success', $message);
        } else {
            set_alert('warning', $message);
        }
        redirect(admin_url('payroll/editpay/'.$payid.''));
    }

    public function payreport()
    {
        $data['staff'] = $this->staff_model->get('', ['active' => 1]);
        $data['client_data'] = $this->Clients_model->get_all_customer();
        $data['fetch_data'] = $this->payroll_model->get_all_payroll();
        $this->load->view('admin/payrolls/payrolls_report', $data);
    }

    public function search_payreport()
    {
        if ($this->input->post()) {
            $data = $this->input->post();
            if($data['from'] == 'From') 
            { $data['from'] = ''; }
            if($data['to'] == 'To')
            { $data['to'] = ''; }

            // echo '<pre>'; print_r($data); echo '</pre>'; exit;
            $data['staff'] = $this->staff_model->get('', ['active' => 1]);
            $data['client_data'] = $this->clients_model->get_all_customer();
            $data['search_data'] = $this->payroll_model->search_pay_report($data);
            $this->load->view('admin/payrolls/search_payrolls', $data);
        }
    }

    public function addfedform()
    {
        if($this->input->post())
        {
            $data['form_name'] = $this->input->post('fedform');
            $data['dateadded']  = date('Y-m-d h:i:s');
            $this->db->insert(db_prefix() . 'federal_forms', $data);
            redirect(admin_url('payroll'));
        }
    }

    public function addstateform()
    {
        if($this->input->post())
        {
            $data['state'] = $this->input->post('state');
            $data['form'] = $this->input->post('form');
            $data['state_type'] = NULL;
            $data['dateadded']  = date('Y-m-d h:i:s');
            // echo '<pre>'; print_r($data); echo '</pre>'; exit;
            $this->db->insert(db_prefix() . 'state_forms', $data);
            redirect(admin_url('payroll'));
        }
    }

    public function preview()
    {
        if (!has_permission('payrolls', '', 'create')) {
            access_denied('payrolls');
        }
        if ($this->input->post()) {
            $sess_data = array(
               'client'          => $this->input->post('client'),
               'federal_form'    => $this->input->post('federal_form'),
               'federal_status'  => $this->input->post('federal_status'),
               'priority'        => $this->input->post('priority'),
               'assigned_to'     => $this->input->post('assigned_to'),
               'startdate'       => $this->input->post('startdate'),
               'duedate'         => $this->input->post('duedate'),
               'stateform'       => $this->input->post('stateform'),
               'reminder_desc'   => $this->input->post('reminder_description'),
               'staffemail'      => $this->input->post('staffemail'),
               'reminder_date'   => $this->input->post('reminder_date'),
               'remarks'         => $this->input->post('remarks'),
               'followers'       => $this->input->post('followers'),
            );
            $this->session->set_userdata($sess_data);
            //echo '<pre>'; print_r($sess_data); echo '</pre>';exit;
          }
        
        $this->load->view('admin/payrolls/preview_payroll',$sess_data);
    }

    public function paypdfdetail($id)
    {
        if (!has_permission('payrolls', '', 'view')) {
            if (!has_permission('payrolls', '', 'create')) {
                access_denied('payrolls');
            }
        }

        $this->load->library('pdf');
        $data['fetch_data'] = $this->payroll_model->get_payroll_byid($id);
        $data['followers'] = $this->payroll_model->get_followers_by_payrollid($id);
        $data['reminder'] = $this->payroll_model->get_reminder_by_payrollid($id);
        $data['states'] = $this->payroll_model->get_states_by_payrollid($id);
        //echo '<pre>'; print_r($data['fetch_data']); echo '</pre>';exit;

        $html = $this->load->view('admin/payrolls/pdfpaydetails', $data, true);

        $dompdf = new Dompdf\Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $pdf = $dompdf->output();
        $dompdf->stream('PayrollDetail.pdf');
    }
}
