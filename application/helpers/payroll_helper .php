<?php

defined('BASEPATH') or exit('No direct script access allowed');


function get_payroll_status_by_id($id)
{
    $CI       = &get_instance();
    $statuses = $CI->payroll_model->get_pay_status();
    //echo "<pre>";print_r($statuses);die;

    $status = [
        'id'         => 0,
        'bg_color'   => '#000',
        'text_color' => '#333',
        'name'       => '[Status Not Found]',
        'order'      => 1,
    ];

    foreach ($statuses as $s) {
        if ($s['id'] == $id) {
            $status = $s;

           // break;
        }
    }

    return $status;
}