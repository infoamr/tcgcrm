<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_hidden('project_id',$task->id); ?>
<div class="panel_s">
    <div class="panel-body">
       <h3 class="bold mtop10 project-name pull-left" style="color: #051e28; font-weight: 600"><?php echo $task->name; ?>
            <span style="color:<?php echo $project_status['color']; ?>; font-size:16px;">
              <?php echo '('.$task_status['name'].')' ?></span>
       </h3>
        <?php if($task->settings->view_tasks == 1 && $task->settings->create_tasks == 1){ ?>
        <a href="<?php echo site_url('clients/task/'.$task->id.'?group=new_task'); ?>" class="btn btn-info pull-right mtop5"><?php echo _l('new_task'); ?></a>
        <?php } ?>
   </div>
</div>
<div class="panel_s">
    <div class="panel-body">
        <?php get_template_part('tasks/task_tabs'); ?>
        <div class="clearfix mtop15"></div>
        <?php get_template_part('tasks/'.$group); ?>
    </div>
</div>