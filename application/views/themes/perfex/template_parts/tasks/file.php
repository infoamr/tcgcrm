<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade _project_file" tabindex="-1" role="dialog" data-toggle="modal">
   <div class="modal-dialog full-screen-modal" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" onclick="close_modal_manually('._project_file'); return false;"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $file->subject; ?></h4>
         </div>
         <div class="modal-body">
            <div class="row">
               
               <div class="col-md-4 project_file_discusssions_area">
                  <div id="project-file-discussion" class="tc-content"></div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="close_modal_manually('._project_file'); return false;"><?php echo _l('close'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
   var discussion_id = '<?php echo $file->id; ?>';
   var discussion_user_profile_image_url = '<?php echo $discussion_user_profile_image_url; ?>';
   var current_user_is_admin = '<?php echo $current_user_is_admin; ?>';
   $('body').on('shown.bs.modal', '._project_file', function() {
     var content_height = ($('body').find('._project_file .modal-content').height() - 165);
     if($('iframe').length > 0){
       $('iframe').css('height',content_height);
     }
     $('.project_file_area,.project_file_discusssions_area').css('height',content_height);
   });
   $('body').find('._project_file').modal({show:true, backdrop:'static', keyboard:false});
</script>
