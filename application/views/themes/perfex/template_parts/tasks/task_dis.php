<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- Discussion bellow -->

<div class="modal-body">
   <div class="row">
      <div class="col-md-12 task-single-col-left">
         <div class="clearfix"></div>
         <div class="clearfix"></div>
         <hr class="hr-10" />
         <div class="clearfix"></div>
         <h4 class="th font-medium mbot15 pull-left"><?php echo _l('task_view_description'); ?></h4>
         <div class="clearfix"></div>
         <?php if(!empty($task->description)){
            echo '<div class="tc-content"><div id="task_view_description">' .check_for_links($task->description) .'</div></div>';
            } else {
            echo '<div class="no-margin tc-content task-no-description" id="task_view_description"><span class="text-muted">' . _l('task_no_description') . '</span></div>';
            } ?>
         <div class="clearfix"></div>
         <hr />
         <a href="#" onclick="add_task_checklist_item('<?php echo $task->id; ?>', undefined, this); return false" class="mbot10 inline-block">
         <span class="new-checklist-item"><i class="fa fa-plus-circle"></i>
         <?php echo _l('add_checklist_item'); ?>
         </span>
         </a>
         <div class="form-group no-mbot checklist-templates-wrapper simple-bootstrap-select task-single-checklist-templates<?php if(count($checklistTemplates) == 0){echo ' hide';}  ?>">
            <select id="checklist_items_templates" class="selectpicker checklist-items-template-select" data-none-selected-text="<?php echo _l('insert_checklist_templates') ?>" data-width="100%" data-live-search="true">
               <option value=""></option>
               <?php foreach($checklistTemplates as $chkTemplate){ ?>
               <option value="<?php echo $chkTemplate['id']; ?>">
                  <?php echo $chkTemplate['description']; ?>
               </option>
               <?php } ?>
            </select>
         </div>
         <div class="clearfix"></div>
         <p class="hide text-muted no-margin" id="task-no-checklist-items"><?php echo _l('task_no_checklist_items_found'); ?></p>
         <div class="row checklist-items-wrapper">
            <div class="col-md-12 ">
               <div id="checklist-items">
                  <?php $this->load->view('admin/tasks/checklist_items_template',
                     array(
                       'task_id'=>$task->id,
                       'checklists'=>$task->checklist_items)); ?>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
          <?php if(count($task->attachments) > 0){ ?>
         <div class="row task_attachments_wrapper">
            <div class="col-md-12" id="attachments">
               <hr />
               <h4 class="th font-medium mbot5"><?php echo _l('task_view_attachments'); ?></h4>
               <div class="col-md-12" style="margin:0px; padding: 0px">
                  <!-- Table For Attachment -->
              <div class="clearfix"></div>
               <!-- Table -->
              <table class="table  table-tasks" data-order-col="3" data-s-type='[{"column":3,"type":"task-status"}]' data-order-type="asc">
                  <thead>
                     <tr>
                        <th>Uploaded By</th>
                        <th>Uploaded On</th>
                        <th>File Name</th>
                        <th></th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php
                     $i = 1;
                     // Store all url related data here
                     $comments_attachments = array();
                     $attachments_data = array();
                     $show_more_link_task_attachments = hooks()->apply_filters('show_more_link_task_attachments', 3);
                     foreach($task->attachments as $attachment){ ?>
                      <?php ob_start(); ?>
                     <tr data-num="<?php echo $i; ?>" data-commentid="<?php echo $attachment['comment_file_id']; ?>" data-comment-attachment="<?php echo $attachment['task_comment_id']; ?>" data-task-attachment-id="<?php echo $attachment['id']; ?>" class="task-attachment-col<?php if($i > $show_more_link_task_attachments){echo ' hide task-attachment-col-more';} ?>" data-title="<?php echo $attachment['file_name']; ?>">
                      <?php 
                        $externalPreview = false;
                        $is_image = false;
                        $path = get_upload_path_by_type('task') . $task->id . '/'. $attachment['file_name'];
                        $href_url = site_url('download/file/taskattachment/'. $attachment['attachment_key']);
                        $isHtml5Video = is_html5_video($path);
                      ?>      
                        <?php
                        if($attachment['staffid'] != 0)
                        { ?><td><?php echo '<a href="'.admin_url('profile/'.$attachment['staffid']).'" target="_blank">'.get_staff_full_name($attachment['staffid']) .'</a>';?></td><?php } 
                        elseif ($attachment['contact_id'] != 0){?>
                        <td><?php echo '<a href="'.admin_url('clients/client/'.get_user_id_by_contact_id($attachment['contact_id']).'?contactid='.$attachment['contact_id']).'" target="_blank">'.get_contact_full_name($attachment['contact_id']) .'</a>';?></td>
                        <?php } ?>
                        <td><?php echo '<span class="text-has-action" data-toggle="tooltip" data-title="'._dt($attachment['dateadded']).'">'._dt($attachment['dateadded']).'</span>'; ?>
                        </td>
                        <?php
                           // Not link on video previews because on click on the video is opening new tab
                        if(!$isHtml5Video){ ?>
                        <td>
                           <a href="<?php echo (!$externalPreview ? $href_url : $externalPreview); ?>" target="_blank"<?php if($is_image){ ?> data-lightbox="task-attachment"<?php } ?> class="<?php if($isHtml5Video){echo 'video-preview';} ?>">
                        <?php } ?>
                        <?php if($isHtml5Video) { ?>
                           <video width="100%" height="100%" src="<?php echo site_url('download/preview_video?path='.protected_file_url_by_path($path).'&type='.$attachment['filetype']); ?>" controls>
                              Your browser does not support the video tag.
                           </video>
                        <?php } else { ?>
                           <i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i>
                           <?php echo $attachment['file_name']; ?>
                           <?php } ?>
                           <?php if(!$isHtml5Video){ ?>
                           </a>
                        </td>
                        <?php } ?>
                        <?php if($attachment['staffid'] == get_staff_user_id() || is_admin()){ ?>
                        <td> 
                        <a href="#" class="pull-right" onclick="remove_task_attachment(this,<?php echo $attachment['id']; ?>); return false;">
                        <i class="fa fa fa-times"></i>
                        </a>
                        </td><?php }  ?>
                      </tr>
                      <?php
                     $attachments_data[$attachment['id']] = ob_get_contents();
                     if($attachment['task_comment_id'] != 0) {
                      $comments_attachments[$attachment['task_comment_id']][$attachment['id']] = $attachments_data[$attachment['id']];
                     }
                     ob_end_clean();
                     echo $attachments_data[$attachment['id']];
                     ?>
                        <?php $i++; } ?>
                  </tbody>
              </table>
              </div>
            </div>
               <!-- Table -->
            
            <?php if(($i - 1) > $show_more_link_task_attachments){ ?>
                <hr / style="padding-left: 10px;padding-right: 10px">
            <div class="col-md-12 text-center" id="show-more-less-task-attachments-col">
               <a href="#" class="btn btn-info task-attachments-more pull-left" onclick="slideToggle('.task_attachments_wrapper .task-attachment-col-more', task_attachments_toggle); return false;"><?php echo _l('show_more'); ?></a>
               <a href="#" class="btn btn-info task-attachments-less pull-left hide" onclick="slideToggle('.task_attachments_wrapper .task-attachment-col-more', task_attachments_toggle); return false;"><?php echo _l('show_less'); ?></a>
                <a href="<?php echo site_url('clients/download_files/'.$task->id); ?>" class="btn btn-success bold pull-right">
               <?php echo _l('download_all'); ?> (.zip)</a>
            </div>
            <?php } ?>
         </div>
         <?php } ?>
         <hr / style="padding-left: 10px;padding-right: 10px">
         <a href="#" id="taskCommentSlide" onclick="slideToggle('.tasks-comments'); return false;">
            <h4 class="mbot20 font-medium"><?php echo _l('task_comments'); ?></h4>
         </a>
         <div class="tasks-comments inline-block full-width simple-editor"<?php if(count($task->comments) == 0){echo ' style="display:none"';} ?>>
            <?php //if(count($task->comments) > 0){echo '<hr />';} ?>
            <div id="task-comments" class="mtop10">
               <?php
                  $comments = '';
                  $len = count($task->comments);
                  $i = 0;
                  foreach ($dis as $comment) {
                    $comments .= '<div id="comment_'.$comment['id'].'" data-commentid="' . $comment['id'] . '" data-task-attachment-id="'.$comment['file_id'].'" class="tc-content task-comment'.(strtotime($comment['dateadded']) >= strtotime('-16 hours') ? ' highlight-bg' : '').'">';
                    $comments .= '<a data-task-comment-href-id="'.$comment['id'].'" href="'.admin_url('tasks/view/'.$task->id).'#comment_'.$comment['id'].'" class="task-date-as-comment-id"><small><span class="text-has-action inline-block mbot5" data-toggle="tooltip" data-title="'._dt($comment['dateadded']).'">' . _dt($comment['dateadded']) . '</span></small></a>';
                    if($comment['staffid'] != 0){
                     $comments .= '<a href="' . admin_url('profile/' . $comment['staffid']) . '" target="_blank">' . staff_profile_image($comment['staffid'], array(
                      'staff-profile-image-small',
                      'media-object img-circle pull-left mright10'
                   )) . '</a>';
                  } elseif($comment['contact_id'] != 0) {
                     $comments .= '<img src="'.contact_profile_image_url($comment['contact_id']).'" class="client-profile-image-small media-object img-circle pull-left mright10">';
                  }
                 
                  $comments .= '<div class="media-body">';
                  if($comment['staffid'] != 0){
                   $comments .= '<a>' . get_staff_full_name($comment['staffid']) . '</a>';
                  } elseif($comment['contact_id'] != 0) {
                   $comments .= '<span class="label label-info mtop5 mbot5 inline-block">&nbsp;'._l('is_customer_indicator').'<a class="pull-left">' . get_contact_full_name($comment['contact_id']) . ' - </a></span> <br />';
                  }
                  $comments .= '<div data-edit-comment="'.$comment['id'].'" class="hide edit-task-comment"><textarea rows="5" id="task_comment_'.$comment['id'].'" class="ays-ignore form-control">'.str_replace('[task_attachment]', '', $comment['content']).'</textarea>
                  <div class="clearfix mtop20"></div>
                  <button type="button" class="btn btn-info pull-right" onclick="save_edited_comment('.$comment['id'].','.$task->id.')">'._l('submit').'</button>
                  <button type="button" class="btn btn-default pull-right mright5" onclick="cancel_edit_comment('.$comment['id'].')">'._l('cancel').'</button>
                  </div>';
                  if($comment['file_id'] != 0){
                  $comment['content'] = str_replace('[task_attachment]','<div class="clearfix"></div>'.$attachments_data[$comment['file_id']],$comment['content']);
                  // Replace lightbox to prevent loading the image twice
                  $comment['content'] = str_replace('data-lightbox="task-attachment"','data-lightbox="task-attachment-comment-'.$comment['id'].'"',$comment['content']);
                  } else if(count($comment['attachments']) > 0 && isset($comments_attachments[$comment['id']])) {
                   $comment_attachments_html = '';
                   foreach($comments_attachments[$comment['id']] as $comment_attachment) {
                       $comment_attachments_html .= trim($comment_attachment);
                   }
                   $comment['content'] = str_replace('[task_attachment]','<div class="clearfix"></div>'.$comment_attachments_html,$comment['content']);
                   // Replace lightbox to prevent loading the image twice
                   $comment['content'] = str_replace('data-lightbox="task-attachment"','data-lightbox="task-comment-files-'.$comment['id'].'"',$comment['content']);
                   $comment['content'] .='<div class="clearfix"></div>';
                  }
                  $comments .= '<div class="comment-content mtop10">'.app_happy_text(check_for_links($comment['content'])) . '</div>';
                  $comments .= '</div>';
                  if ($i >= 0 && $i != $len - 1) {
                  $comments .= '<hr class="task-info-separator" />';
                  }
                  $comments .= '</div>';
                  $i++;
                  }
                  echo $comments;
                  ?>
            </div>
            <div class="clearfix"></div>
            <hr />
            <?php
              $attribute = array('name' => 'clientlogin',
              'id'=>'task-file-upload',
              'class'=>'dropzone mtop15',
              //'style'=>'min-height:auto;background-color:#fff',
              'accept-charset'=>'utf-8',
              'role' => 'form',
              'enctype' => 'multipart/form-data',
              'method'=> 'post'); ?>
            <?php echo form_open_multipart(site_url('clients/upload_fi'),$attribute); ?>
            <input type="file" name="file" multiple class="hide"/>
            <input type="hidden" id="custId" name="taskid" value="<?php echo $task->id ?>">
            <input type="hidden" id="contact_id" name="contact_id" value="<?php echo get_contact_user_id(); ?>">
            <?php echo form_close(); ?>
            <hr />
            <?php
            $attributes = array('name' => 'adminlogin',
            'id'=>'task-comment-form',
            //'class'=>'dropzone dropzone-manual',
            'style'=>'min-height:auto;background-color:#fff',
            'accept-charset'=>'utf-8',
            'role' => 'form',
            //'enctype' => 'multipart/form-data',
            'method'=> "POST"); ?>
            <?php echo form_open(site_url('clients/add_text'), $attributes); ?>
            <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
            <?php hooks()->do_action('after_admin_login_form_start'); ?>
            <div class="form-group">
              <textarea name="comment" placeholder="<?php echo _l('task_single_add_new_comment'); ?>" id="task_comment" rows="3" class="form-control ays-ignore"></textarea>
              <!-- <input type="file" style="color:#888888" name="file" id="file" />  -->
              <input type="hidden" id="custId" name="taskid" value="<?php echo $task->id; ?>">
              <input type="hidden" id="staffid" name="staffid" value="<?php echo $attachment['staffid']; ?>">
              <input type="hidden" id="file_id" name="file_id" value="<?php echo $task->client_id; ?>">
              <input type="hidden" id="contact_id" name="contact_id" value="<?php echo $contacts->id; ?>">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-info mtop10 pull-right" name="addcomment">ADD COMMENT</button>
            </div>
            <?php hooks()->do_action('before_admin_login_form_close'); ?>
            <?php echo form_close(); ?>

         </div>
      </div>
   </div>
</div>
<script>
   if(typeof(commonTaskPopoverMenuOptions) == 'undefined') {
      var commonTaskPopoverMenuOptions = {
         html: true,
         placement: 'bottom',
         trigger: 'click',
         template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"></div></div></div>',
      };
   }

   // Clear memory leak
   if(typeof(taskPopoverMenus) == 'undefined'){
      var taskPopoverMenus = [{
            selector: '.task-menu-options',
            title: "<?php echo _l('actions'); ?>",
         },
         {
            selector: '.task-menu-status',
            title: "<?php echo _l('ticket_single_change_status'); ?>",
         },
         {
            selector: '.task-menu-priority',
            title: "<?php echo _l('task_single_priority'); ?>",
         },
         {
            selector: '.task-menu-milestones',
            title: "<?php echo _l('task_milestone'); ?>",
         },
      ];
   }

   for (var i = 0; i < taskPopoverMenus.length; i++) {
      $(taskPopoverMenus[i].selector + ' .trigger').popover($.extend({}, commonTaskPopoverMenuOptions, {
         title: taskPopoverMenus[i].title,
         content: $('body').find(taskPopoverMenus[i].selector + ' .content-menu').html()
      }));
   }

   if (typeof (Dropbox) != 'undefined') {
      document.getElementById("dropbox-chooser-task").appendChild(Dropbox.createChooseButton({
         success: function (files) {
            taskExternalFileUpload(files,'dropbox',<?php echo $task->id; ?>);
         },
         linkType: "preview",
         extensions: app.options.allowed_files.split(','),
      }));
   }

   init_selectpicker();
   init_datepicker();
   init_lightbox();

   tinyMCE.remove('#task_view_description');

   if (typeof (taskAttachmentDropzone) != 'undefined') {
      taskAttachmentDropzone.destroy();
      taskAttachmentDropzone = null;
   }

   taskAttachmentDropzone = new Dropzone("#task-attachment", appCreateDropzoneOptions({
      uploadMultiple: true,
      parallelUploads: 20,
      maxFiles: 20,
      paramName: 'file',
      sending: function (file, xhr, formData) {
         formData.append("taskid", '<?php echo $task->id; ?>');
      },
      success: function (files, response) {
         response = JSON.parse(response);
         if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
            _task_append_html(response.taskHtml);
         }
      }
   }));

   $('#task-modal').find('.gpicker').googleDrivePicker({ onPick: function(pickData) {
      taskExternalFileUpload(pickData, 'gdrive', <?php echo $task->id; ?>);
   }});

</script>
