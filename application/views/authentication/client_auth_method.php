<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php $this->load->view('authentication/includes/head.php'); ?>
<body class="authentication two-factor-authentication-code">
 <div class="container">
  <div class="row">
   <div class="col-md-4 col-md-offset-4 authentication-form-wrapper">
   <div class="company-logo">
     <?php echo get_company_logo(); ?>
   </div>
   <div class="mtop40 authentication-form">
    <h1>2-STEP VERIFICATION<br /><small>Choose an option to get code</small></h1>
    <?php echo form_open(site_url('authentication/twostep_verify')); ?>
    <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
    <?php //$this->load->view('authentication/includes/alerts'); ?>

    <div class="form-group">
      <input type="radio" name="authtype" value="1">
      <label><b>Phone (<?php
        $CI = &get_instance();
        $ct = $CI->db->select('phonenumber')->where('userid',$clientid)
        ->get(db_prefix().'contacts')->result();
        echo '******'.substr($ct[0]->phonenumber, -4); ?>)
      </b></label>
      <p>OR</p>
    </div>

    <div class="form-group">
      <input type="radio" name="authtype" value="2">
      <label><b>Email (<?php 
        $CI = &get_instance();
        $em = $CI->db->select('email')->where('userid',$clientid)
        ->get(db_prefix().'contacts')->result();
        $email =  strtok($em[0]->email, '@');
        $allTheRest = strtok( '' ); 
        echo substr($email, 0,3).'****'.substr($email, -2).'@'.$allTheRest; ?>)
      </b></label>
    </div>

    <div class="form-group">
      <a href="<?php echo site_url('authentication'); ?>"><?php echo _l('back_to_login'); ?></a>
    </div>

    <div class="form-group">
      <input type="hidden" name="phonenum" value="<?php echo $ct[0]->phonenumber; ?>">
      <button type="submit" class="btn btn-info btn-block">Submit</button>
    </div>
    <?php echo form_close(); ?>
  </div>
</div>
</div>
</div>
</body>
</html>