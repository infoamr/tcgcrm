<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php $this->load->view('authentication/includes/head.php'); ?>
<body class="authentication two-factor-authentication-code">
 <div class="container">
  <div class="row">
   <div class="col-md-4 col-md-offset-4 authentication-form-wrapper">
   <div class="company-logo">
     <?php echo get_company_logo(); ?>
   </div>
   <div class="mtop40 authentication-form">
    <h1>2-STEP VERIFICATION<br /><small>Choose an option to get code</small></h1>
    <?php echo form_open(admin_url('authentication/twostep_verify/'.$staffid)); ?>
    <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
    <?php //$this->load->view('authentication/includes/alerts'); ?>

    <div class="col-md-12">
      <div class="col-md-2" style="text-align:right">
        <input type="radio" name="authtype" value="1">
      </div>
      <div class="col-md-10">
        <label><b>Phone (<?php
          $CI = &get_instance();
          $std = $CI->db->select('*')->where('staffid',$staffid)->get(db_prefix().'staff')->result();
          echo '******'.substr($std[0]->phonenumber, -4); ?>)
        </b></label>
      </div>
    </div>
    <div class="col-md-12" style="padding-left: 120px">OR</div>
    <div class="col-md-12" style="padding-top: 6px">
      <div class="col-md-2" style="text-align:right">
        <input type="radio" name="authtype" value="2">
      </div>
      <div class="col-md-10" style="padding-right: 0px!important">
        <label><b>Email (<?php 
          $email =  strtok($std[0]->email, '@');
          $dom = substr($std[0]->email, strrpos($std[0]->email, '@' )+1);
          echo substr($email, 0,3).'*****'.'@'.$dom; ?>)
       </b></label>
      </div>
    </div>
    <div class="col-md-12">
      <hr class="no-mtop"/>
    </div>
    <div class="form-group">
      <a href="<?php echo admin_url('authentication'); ?>"><?php echo _l('back_to_login'); ?></a>
    </div>
    <div class="form-group">
      <input type="hidden" name="stid" value="<?php echo $staffid; ?>">
      <input type="hidden" name="phonenum" value="<?php echo $std[0]->phonenumber; ?>">
      <button type="submit" class="btn btn-info btn-block">Submit</button>
    </div>
    <?php echo form_close(); ?>
  </div>
</div>
</div>
</div>
</body>
</html>