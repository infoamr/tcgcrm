<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php $this->load->view('authentication/includes/head.php'); ?>
<body class="authentication two-factor-authentication-code">
 <div class="container">
  <div class="row">
   <div class="col-md-4 col-md-offset-4 authentication-form-wrapper">
   <div class="company-logo">
     <?php echo get_company_logo(); ?>
   </div>
   <div class="mtop40 authentication-form">
    <h1>PHONE VERIFICATION<br /><small>Enter otp</small></h1>
    <?php echo form_open(admin_url('authentication/verifyotp')); ?>
    <?php //echo form_open($this->uri->uri_string()); ?>
    <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
    <?php if(isset($otp_id)) echo '<div class="alert alert-success text-center">Otp sent successfully.</div>'?>
    <?php if(isset($error)) echo '<div class="alert alert-danger text-center">Enter correct otp or resend new otp.</div>'?>
    <?php echo render_input('code','two_factor_authentication_code'); ?>
    <div class="form-group">
      <a href="<?php echo admin_url('authentication/resendotp/'.$otp_id); ?>">Resend OTP</a>
    </div>
    <div class="form-group">
      <input type="hidden" name="otp_id" value="<?php echo $otp_id; ?>">
      <button type="submit" class="btn btn-info btn-block"><?php echo _l('confirm'); ?></button>
    </div>
    <?php echo form_close(); ?>
  </div>
</div>
</div>
</div>
</body>
</html>
