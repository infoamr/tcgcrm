<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-4">
        <div class="panel_s">
          <div class="panel-body">
            <div style="font-weight: bold; text-align: center;">
              <?php 
                $CI = &get_instance();
                $CI->db->select('count(id) as num');
                $CI->db->like('dateadded', date('Y-m-d'));
                $CI->db->from('tbltasks');
                $todcnt = $CI->db->get()->result();
                echo 'Total Works on '. date('d F Y').' is '.$todcnt[0]->num;
              ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <hr class="no-mtop"/>
            </div>

            <table class="table table-striped dt-table scroll-responsive" data-order-col="1" data-order-type="desc">
              <thead>
                <tr>
                  <th>Assigned Staff</th> 
                  <th>Today Works Count</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $CI = &get_instance();
                  $CI->db->select('staffid, CONCAT(firstname, " ", lastname) as fullname');
                  $staff = $CI->db->get(db_prefix() . 'staff')->result_array();

                  foreach($staff as $st) { 
                    $CI = &get_instance();
                    $CI->db->select('count(tbltask_assigned.id) as number, tbltask_assigned.staffid, tbltasks.task_number, tbltask_assigned.taskid');
                    $CI->db->like('tbltasks.dateadded', date('Y-m-d'));
                    $CI->db->where('tbltask_assigned.staffid', $st['staffid']);
                    $CI->db->from('tbltask_assigned');
                    $CI->db->join('tbltasks', 'tbltasks.id = tbltask_assigned.taskid');
                    $todaytask = $CI->db->get()->result();
                    echo '<tr><td>'.$st['fullname'].'</td><td>'.$todaytask[0]->number.'</td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="col-md-8">
        <div class="panel_s">
          <div class="panel-body">
              <div class="clearfix"></div>
              <div class="row">
                <div class="col-md-12">
                 <?php echo form_open(admin_url('tasks/staff_works_count')); ?>
                 <div class="col-md-3">
                    <?php echo render_date_input('from','','From'); ?>
                 </div>

                 <div class="col-md-3">
                    <?php echo render_date_input('to','','To'); ?>
                 </div>
                 
                 <div class="col-md-3">
                  <div class="select-placeholder">
                     <select name="assigned_to" id="assigned_to" class="selectpicker" data-live-search="true" data-width="100%">
                        <option value=""><?php echo _l('all_staff_members'); ?></option>
                        <?php foreach($staff as $st){ ?>
                        <option value="<?php echo $st['staffid']; ?>"><?php echo $st['fullname']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
                 </div>

                 <div class="col-md-3">
                  <button type="submit" class="btn btn-info pull-left"><?php echo _l('apply'); ?></button>
                 </div>
                <?php echo form_close(); ?>
                </div>

                <div class="col-md-12">
                  <hr class="no-mtop"/>
                </div>

                <div class="col-md-12">
                  <?php $msg = '';
                    if(!empty($from) && !empty($to)) {
                      $newfrom =  date('d F Y', strtotime($from));
                      $toDt = date('Y-m-d', strtotime($to . ' -1 day'));
                      $newto =  date('d F Y', strtotime($toDt));
                      $msg='From '.$newfrom.' to '.$newto;
                    }
                    if(!empty($from) && empty($to)) {
                      $newfrom =  date('d F Y', strtotime($from));
                      $msg='On '.$newfrom;
                    }
                    if(!empty($to) && empty($from)) {
                      $newto =  date('d F Y', strtotime($to));
                      $msg='On '.$newto;
                    } ?>
                  <span style="font-weight: bold; text-align: center;"><?php echo $msg;?></span>
                  <br>
                  <span style="font-weight: bold;">Total works is <?php echo count($count);?></span>
                  <?php if(!empty($assigned_to)) { ?>
                  <br>
                  <span style="font-weight: bold;"><?php 
                    $CI = &get_instance();
                    $CI->db->select('CONCAT(firstname, " ", lastname) as staffname');
                    $CI->db->where('staffid', $assigned_to);
                    $res = $CI->db->get(db_prefix() . 'staff')->result();
                    echo $res[0]->staffname.' works is '.count($srdata); ?></span>
                  <?php } ?>
                </div>

                <div class="col-md-12">
                  <hr class="no-mtop"/>
                </div>

              </div>
              <div class="clearfix"></div>
              
              <?php if(empty($assigned_to)) { 
                  echo '<table class="table table-striped dt-table scroll-responsive"><thead><tr><th>Assigned Staff</th><th>Works Count</th></tr></thead><tbody>';
                  foreach($staff as $st) { 
                    if(!empty($from) && !empty($to)) {
                      //$to = date('Y-m-d', strtotime($to . ' +1 day'));
                      $this->db->where('tasks.dateadded >= ', $from);
                      $this->db->where('tasks.dateadded <= ', $to);
                    }
                    if(!empty($from) && empty($to)) {
                      $this->db->like('tasks.dateadded', $from);
                    }
                    if(!empty($to) && empty($from)) {
                      $this->db->like('tasks.dateadded', $to);
                    }
                    
                    $this->db->where('task_assigned.staffid', $st['staffid']);
                    $this->db->select('tasks.id as tid');
                    $this->db->from(db_prefix().'tasks');
                    $this->db->join(db_prefix().'task_assigned', 'task_assigned.taskid = tasks.id');
                    $res = $this->db->get()->result_array();
                    echo '<tr><td>'.$st['fullname'].'</td><td>'.count($res).'</td></tr>';
                  }
                  //echo '<pre>'; print_r($this->db->last_query()); echo '</pre>'; exit;
                }
                else {
                  echo '<table class="table table-striped dt-table scroll-responsive" data-order-col="2" data-order-type="desc"><thead><tr><th>Assigned Staff</th><th>Task Number</th><th>Created Date</th></tr></thead><tbody>';
                  foreach ($srdata as $val) {
                    $CI = &get_instance();
                    $CI->db->select('CONCAT(firstname, " ", lastname) as fullname');
                    $CI->db->where('staffid', $val['sfid']);
                    $qry = $CI->db->get(db_prefix() . 'staff')->result();
                  echo '<tr><td>'.$qry[0]->fullname.'</td><td>'.$val['tnum'].'</td><td>'._d($val['dateadded']).'</td></tr>';
                  }
                } ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
</body>
</html>
