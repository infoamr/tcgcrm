<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="staff_group_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title">Edit Staff Group</span>
                    <span class="add-title">Add New Staff Group</span>
                </h4>
            </div>
            <?php echo form_open('admin/staff/group',array('id'=>'staff-group-modal')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('name','customer_group_name'); ?>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    <?php echo '<h5>Groups Members</h5><hr>';
                    if(has_permission('tasks','','edit') || has_permission('tasks','','create')){ ?>
                     <div class="simple-bootstrap-select">
                        <select data-width="100%" id="add_group_staff" class="text-muted task-action-select selectpicker" name="select-staff" data-live-search="true" title='Add Staff' data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                        <?php $tgar = array();
                            $staff = $this->db->select('staffid, firstname, lastname')->get(db_prefix() . 'staff')->result_array();
                            $grp_members = $this->db->select('staff.staffid, staff.firstname, staff.lastname, staff.email')->where('staff_groups.groupid',1)->join(db_prefix() . 'staff_groups', db_prefix() . 'staff_groups.staff_id = ' . db_prefix() . 'staff.staffid')->get(db_prefix() . 'staff')->result_array();
                            foreach($grp_members as $grm) {
                                $tgar[] = $grm['staffid']; 
                            }
                           $options = '';
                           foreach ($staff as $grp) {
                              if (!in_array($grp['staffid'],$tgar)) {
                                 $options .= '<option value="' . $grp['staffid'] . '">'.$grp['firstname'].' '.$grp['lastname'].'</option>';
                              }
                           }
                           echo $options;
                        ?>
                        </select>
                     </div>
                     <?php }
                      echo '<br><div class="container-fluid">';
                      echo '<div class="row">';
                      foreach ($grp_members as $gm) {
                      echo '<div class="col-sm-3" style="background-color:lavender; border: 1px solid black; border-radius: 10px;"><span style="font-size:12px;">'.$gm['firstname'].' '.$gm['lastname'].'</span></div>';
                      }
                        echo '</div>';
                        echo '</div>';
                    ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<script>
    window.addEventListener('load',function(){
       appValidateForm($('#staff-group-modal'), {
        name: 'required'
    }, manage_staff_groups);

       $('#staff_group_modal').on('show.bs.modal', function(e) {
        var invoker = $(e.relatedTarget);
        var group_id = $(invoker).data('id'); 
        $('#staff_group_modal .add-title').removeClass('hide');
        $('#staff_group_modal .edit-title').addClass('hide');
        $('#staff_group_modal input[name="id"]').val('');
        $('#staff_group_modal input[name="name"]').val('');
        // is from the edit button
        if (typeof(group_id) !== 'undefined') {
            $('#staff_group_modal input[name="id"]').val(group_id);
            $('#staff_group_modal .add-title').addClass('hide');
            $('#staff_group_modal .edit-title').removeClass('hide');
            $('#staff_group_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
        }
    });
   });
    function manage_staff_groups(form) {
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-staff-groups')) {
                    $('.table-staff-groups').DataTable().ajax.reload();
                }
                if($('body').hasClass('dynamic-create-groups') && typeof(response.id) != 'undefined'){
                    var groups = $('select[name="groups_in[]"]');
                    groups.prepend('<option value="'+response.id+'">'+response.name+'</option>');
                    groups.selectpicker('refresh');
                }
                alert_float('success', response.message);
            }
            $('#staff_group_modal').modal('hide');
        });
        return false;
    }

</script>
