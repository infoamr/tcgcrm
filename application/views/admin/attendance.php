<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<style>
p {
  text-align: center;
  font-size: 60px;
  margin-top: 0px;
}
</style>
<div id="wrapper">
   <div class="content">
      <div class="row" style="margin-right: -15px;
    margin-left: -15px;
    text-align: center;
    height: 234px;
    vertical-align: middle;
    margin-top: 100px;">
         <div class="col-md-12">
          <h1><?php echo date('l d F Y'); ?></h1>
          <?php 
            $CI = &get_instance();
            $check = $CI->db->select('*')
                    ->where('staff_id', get_staff_user_id())
                    ->where('date', date('Y-m-d'))
                    ->get(db_prefix() . 'stafftimers')->result();

            if(empty($check)) { ?>
              <a href="<?php echo admin_url('dashboard/intime'); ?>" class="btn btn-success">Login</a>
           <?php } 
            else { 
              if($check[0]->end_time != NULL) { ?>
                <h3><?php echo 'In time '.date('d F Y, g:i A',$check[0]->start_time);?></h3>
                <h3><?php echo 'Out time '.date('d F Y, g:i A',$check[0]->end_time);?></h3>
             <?php }
              else { ?>
                <h3><?php echo 'You logged in at '.date('d F Y, g:i A',$check[0]->start_time);?></h3>
                <p id="logtm"></p>
                <p id="totaltm"></p>
                <a href="<?php echo admin_url('dashboard/outtime/'.$check[0]->id.''); ?>" class="btn btn-success">Logout</a>
           <?php }
            } ?>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>

<script>
// Set the date we're counting down to
var dt = "<?php echo date('F j, Y H:i:s',$check[0]->start_time); ?>";
var intime = new Date(dt).getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
   // alert(intime);
  // alert(now);
    
  // Find the distance between now and the login datetime
  var logintm = now - intime;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(logintm / (1000 * 60 * 60 * 24));
  var hours = Math.floor((logintm % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((logintm % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((logintm % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("logtm").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
    
}, 1000);
</script>

</body>
</html>
