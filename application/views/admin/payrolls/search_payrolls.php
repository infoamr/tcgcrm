<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="row">
                    <div class="col-md-12">
                    <?php echo form_open(admin_url('payroll/search_payreport')); ?>
                     <div class="col-md-2">
                        <?php echo render_date_input('from','','From'); ?>
                     </div>
                     <div class="col-md-2">
                        <?php echo render_date_input('to','','To'); ?>
                     </div>

                     <?php //if(isset($view_all)){ ?>
                     <div class="col-md-3">
                        <div class="select-placeholder">
                           <select name="assigned_to" id="assigned_to" class="selectpicker" data-live-search="true" data-width="100%">
                              <option value=""><?php echo _l('all_staff_members'); ?></option>
                              <?php foreach($staff as $st){ ?>
                              <option value="<?php echo $st['staffid']; ?>"><?php echo $st['full_name']; ?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                     <?php //} ?>

                      <div class="col-md-3">
                        <div class="select-placeholder">
                          <select name="client_id" class="selectpicker" id="client_id" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                             <option value="">All Customers</option>
                             <?php foreach($client_data->result() as $cd) { ?>
                           <option value="<?php echo $cd->userid ?>"><?php echo $cd->company?></option>
                           <?php } ?>
                          </select>
                        </div>
                      </div>
                     
                     <div class="col-md-2">
                      <button type="submit" class="btn btn-info pull-left"><?php echo _l('apply'); ?></button>
                     </div>
                    <?php echo form_close(); ?>
                    </div>

                    <div class="col-md-12">
                      <hr class="no-mtop"/>
                    </div>

                  </div>
                  <div class="clearfix"></div>
                  <table class="table dt-table scroll-responsive" data-order-col="4" data-order-type="desc">
                     <thead>
                     <tr>
                        <th>#Number</th>
                        <th>Company</th>
                        <th>Assigned To</th>
                        <th>Due Date</th>
                        <th>Created Date</th> 
                        <th>Priority</th>
                     </tr>
                     </thead>
                     <tbody>
                      <?php foreach($search_data as $sd) { ?>
                          <tr>
                            <?php 
                            $outputName = '';
                            $outputName .= '<a href=" '.admin_url('payroll/viewpay/' . $sd['id']).'">'.$sd['payroll_number'].'</a><br/>';
                            if ($sd['recurring'] == 1) {
                            $outputName .= '<span class="label label-primary inline-block mtop4"> ' . _l('recurring_tasks') . '</span><br />';
                            } ?>
                            <td><?php echo $outputName;; ?></td>
                            <td><?php echo $sd['company']; ?></td>
                            <td><?php echo $sd['firstname'].' '.$sd['lastname']; ?></td>
                            <td><?php echo _d($sd['duedate']); ?></td>
                            <td><?php echo _d($sd['dateadded']); ?></td>
                            <?php if ($sd['priority'] == 1) {
                                $priority = _l('task_priority_low');
                              } else if ($sd['priority'] == 2) {
                                $priority = _l('task_priority_medium');
                              } else if ($sd['priority'] == 3) {
                                $priority = _l('task_priority_high');
                              } else if ($sd['priority'] == 4) {
                                $priority = _l('task_priority_urgent');
                              } else {
                                $priority = '';
                              }
                            ?>
                            <td><?php echo $priority; ?></td>
                          </tr>
                      <?php } ?>
                     </tbody>
                     </table>
               </div>
         </div>
      </div>
   </div>
</div>
</div>
<?php init_tail(); ?>

<script>
  

</script>

</body>
</html>
