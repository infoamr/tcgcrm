<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
        <?php echo form_open(admin_url('payroll/editpay/'.$fetch_data[0]['id'])); ?>
        <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
          <div class="col-md-12">
            <div class="panel_s">
              <div class="panel-body">
                <p class="customer-profile-group-heading bold6">Edit Payroll (<?php echo $fetch_data[0]['payroll_number'] ?>)</p>
                  <div class="row">
      
                    <div class="col-md-4">
                     <div class="form-group">
                        <p class="bold6">Company</p>
                        <select name="client" class="selectpicker" id="client" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <option value=""></option>
                           <option value="addnew">Add Company</option>
                           <?php foreach($client_data->result() as $data) { ?>
                           <option value="<?php echo $data->userid ?>"<?php if(isset($fetch_data) && $fetch_data[0]['client_id'] == $data->userid){ echo "selected";}?>><?php echo $data->company?></option>
                           <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <p class="bold6">Federal Form</p>
                          <select id="federal_form" name="federal_form" data-width="100%" class="selectpicker">
                            <option value=""></option>
                            <?php foreach($federal_forms as $form) {?>
                            <option value="<?php echo $form['id'] ?>" <?php if(isset($fetch_data) && $fetch_data[0]['federal_form'] == $form['id']){ echo 'selected';}?>><?php echo $form['form_name'] ?></option>
                            <?php } ?>
                          </select>   
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group"> 
                        <p class="bold6">Form Status</p> 
                        <div class="bs3" style="padding-top:5px; padding-bottom:3px;">
                          <div id="federal_status">
                           <input style="padding-left:9px;" type="radio" name="federal_status" value="1" <?php if(isset($fetch_data) && $fetch_data[0]['federal_status'] == 1){ echo "checked";}?>><label style="font-size: 13px;" for="type">&nbsp;Pending</label>

                            <input style="margin-left:9px;" type="radio" name="federal_status" value="2" <?php if(isset($fetch_data) && $fetch_data[0]['federal_status'] == 2){ echo "checked";}?>><label style="font-size: 13px;" for="type">&nbsp;In Progress</label>

                            <input style="margin-left:9px;" type="radio" name="federal_status" value="3" <?php if(isset($fetch_data) && $fetch_data[0]['federal_status'] == 3){ echo "checked";}?>><label style="font-size: 13px;" for="type">&nbsp;Filed</label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="bold6">Priority</p>
                          <select id="priority" name="priority" data-width="100%" class="selectpicker">
                             <option value="1"<?php if(isset($fetch_data) && $fetch_data[0]['priority'] == 1){echo 'selected';} ?>>Low</option>
                            <option value="2"<?php if(isset($fetch_data) && $fetch_data[0]['priority'] == 2){echo 'selected';} ?>>Medium</option>
                            <option value="3"<?php if(isset($fetch_data) && $fetch_data[0]['priority'] == 3){echo 'selected';} ?>>High</option>
                            <option value="4"<?php if(isset($fetch_data) && $fetch_data[0]['priority'] == 4){echo 'selected';} ?>>Urgent</option>
                          </select>   
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="bold6">Assigned To</p>
                       <select name="assigned_to" class="selectpicker" id="assigned_to" data-live-search="true" data-width="100%">
                         <option value=""></option>
                         <?php foreach($staff_data->result() as $staff_data) { ?>
                         <option value="<?php echo $staff_data->staffid; ?>"<?php if(isset($fetch_data) && $fetch_data[0]['assigned_to'] == $staff_data->staffid){echo 'selected';} ?>><?php echo $staff_data->firstname.' '.$staff_data->lastname; ?></option>
                         <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <p class="bold6">Start Date</p>
                      <?php if(isset($fetch_data)){
                        $value = _d($fetch_data[0]['startdate']);
                        } else {
                        $value = _d(date('Y-m-d'));
                        } ?>
                      <?php echo render_date_input('startdate','',$value, ''); ?>
                    </div>

                    <div class="col-md-6">
                      <?php $value = (isset($fetch_data) ? _d($fetch_data[0]['duedate']) : ''); ?>
                      <p class="bold6">Due Date</p>
                      <?php echo render_date_input('duedate','',$value,''); ?>
                    </div>

                    <div class="table-responsive"> 
                      <table class="table table-bordered"> 
                        <thead> 
                          <tr> 
                            <th class="text-center"><p class="bold6">Select</p></th>
                            <th class="text-center"><p class="bold6">State</p></th>
                            <th class="text-center"><p class="bold6">Form</p></th>
                            <th class="text-center"><p class="bold6">Status</p></th>
                          </tr> 
                        </thead> 
                        <tbody id="tbody"> 
                          <?php 
                          $allstates = json_decode($states[0]['state_forms_ids']);
                          $starr = explode(",", $allstates[0]);
                          foreach($state_forms as $stf) { ?>
                            <tr>
                            <td class="text-center">
                              <div class="form-group">
                              <input type="checkbox" id="<?php echo $stf['id'] ?>" name="stateform" value="<?php echo $stf['id'] ?>" <?php if(in_array($stf['id'], $starr)){ echo "checked";}?>>
                              </div>
                            </td>
                            <td class="text-center"><p><?php echo $stf['state'] ?></p></td>
                            <td class="text-center"><p><?php echo $stf['form'] ?></p></td>
                            <td class="text-center"><p><?php echo $stf['state_type'] ?></p></td> 
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div> 

                    <div class="col-md-6">
                        <p class="bold6"><?php echo _l('reminders').' ('._l('set_reminder_date').' )'; ?></p>
                        <?php if(isset($reminder)){
                          $value = _d($reminder[0]['date']);
                          } else {
                          $value = '';
                          } ?>
                        <?php echo render_datetime_input('reminder_date','',$value,array('data-date-min-date'=>_d(date('Y-m-d')))); ?>

                          <!-- <div class="form-group">
                            <p class="bold6">Set reminder to</p>
                            <input type="text" name="reminder_set_to" class="form-control" placeholder="Team Ram <ramandteam@ramassociates.us>, <PKRam@RamAssociates.us>" value="ramandteam@ramassociates.us,PKRam@RamAssociates.us"readonly />
                          </div> -->

                          <div class="form-group">
                              <p class="bold6">Set reminder to</p>
                             <select name="staffemail" class="selectpicker" id="staffemail" data-live-search="true" data-width="100%">
                               <option value=""></option>
                               <?php foreach($staff_email->result() as $em) { ?>
                               <option value="<?php echo $em->email?>"><?php echo $em->email?></option>
                               <?php } ?>
                              </select>
                            </div>

                          <div class="form-group">
                            <p class="bold6">Description</p>
                            <textarea class="form-control" name="reminder_description"></textarea>
                          </div>
                          <!-- <div class="form-group">
                            <div class="checkbox checkbox-primary">
                              <input type="checkbox" name="notify_by_email" id="notify_by_email">
                              <label for="notify_by_email"><?php //echo _l('reminder_notify_me_by_email'); ?></label>
                            </div>
                          </div> -->
                    </div>

                    <?php $payarr = array();
                      foreach($payfollowers as $pf) {
                        foreach($pf as $p) {
                         $payarr[] = $p;
                        }
                    } ?>
                    <div class="col-md-3">
                      <div class="form-group">
                        <p class="bold6">Followers</p>
                       <select name="followers[]" class="selectpicker" id="followers" multiple="true" data-live-search="true" data-width="100%">
                         <option value=""></option>
                         <?php foreach($followers->result() as $fl) { ?>
                         <option value="<?php echo $fl->staffid?>"
                          <?php if(in_array($fl->staffid, $payarr)) {
                             echo 'selected'; }?>
                          ><?php echo $fl->firstname.' '.$fl->lastname; ?></option>
                         <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <p class="bold6">Remarks</p>
                          <input type="text" name="remarks" class="form-control" value="<?php echo $fetch_data[0]['remarks'] ?>" />
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                       <div class="form-group">
                          <p class="bold6"><?php echo _l('task_repeat_every'); ?></p>
                          <select name="repeat_every" id="repeat_every" class="selectpicker" data-width="100%">
                             <option value=""></option>
                             <option value="1-week" <?php if(isset($task) && $task->repeat_every == 1 && $task->recurring_type == 'week'){echo 'selected';} ?>><?php echo _l('week'); ?></option>
                             <option value="2-week" <?php if(isset($task) && $task->repeat_every == 2 && $task->recurring_type == 'week'){echo 'selected';} ?>>2 <?php echo _l('weeks'); ?></option>
                             <option value="1-month" <?php if(isset($task) && $task->repeat_every == 1 && $task->recurring_type == 'month'){echo 'selected';} ?>>1 <?php echo _l('month'); ?></option>
                             <option value="2-month" <?php if(isset($task) && $task->repeat_every == 2 && $task->recurring_type == 'month'){echo 'selected';} ?>>2 <?php echo _l('months'); ?></option>
                             <option value="3-month" <?php if(isset($task) && $task->repeat_every == 3 && $task->recurring_type == 'month'){echo 'selected';} ?>>3 <?php echo _l('months'); ?></option>
                             <option value="6-month" <?php if(isset($task) && $task->repeat_every == 6 && $task->recurring_type == 'month'){echo 'selected';} ?>>6 <?php echo _l('months'); ?></option>
                             <option value="1-year" <?php if(isset($task) && $task->repeat_every == 1 && $task->recurring_type == 'year'){echo 'selected';} ?>>1 <?php echo _l('year'); ?></option>
                             <option value="custom" <?php if(isset($task) && $task->custom_recurring == 1){echo 'selected';} ?>><?php echo _l('recurring_custom'); ?></option>
                          </select>
                       </div>
                    </div>

                    <div class="recurring_custom <?php if((isset($task) && $task->custom_recurring != 1) || (!isset($task))){echo 'hide';} ?>">
                         <div class="col-md-3">
                            <p class="bold6">Count</p>
                            <?php $value = (isset($task) && $task->custom_recurring == 1 ? $task->repeat_every : 1); ?>
                            <?php echo render_input('repeat_every_custom','',$value,'number',array('min'=>1)); ?>
                         </div>
                         <div class="col-md-3">
                            <p class="bold6">Every</p>
                            <select name="repeat_type_custom" id="repeat_type_custom" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                               <option value="day" <?php if(isset($task) && $task->custom_recurring == 1 && $task->recurring_type == 'day'){echo 'selected';} ?>><?php echo _l('task_recurring_days'); ?></option>
                               <option value="week" <?php if(isset($task) && $task->custom_recurring == 1 && $task->recurring_type == 'week'){echo 'selected';} ?>><?php echo _l('task_recurring_weeks'); ?></option>
                               <option value="month" <?php if(isset($task) && $task->custom_recurring == 1 && $task->recurring_type == 'month'){echo 'selected';} ?>><?php echo _l('task_recurring_months'); ?></option>
                               <option value="year" <?php if(isset($task) && $task->custom_recurring == 1 && $task->recurring_type == 'year'){echo 'selected';} ?>><?php echo _l('task_recurring_years'); ?></option>
                            </select>
                         </div>
                    </div>

                    <div id="cycles_wrapper" class="<?php if(!isset($task) || (isset($task) && $task->recurring == 0)){echo ' hide';}?>">
                      <?php $value = (isset($task) ? $task->cycles : 0); ?>
                      <div class="col-md-2">
                        <div class="form-group recurring-cycles">
                           <p class="bold6"><?php echo _l('recurring_total_cycles'); ?></p>
                           <?php if(isset($task) && $task->total_cycles > 0){
                              echo '<small>' . _l('cycles_passed', $task->total_cycles) . '</small>';
                              }
                              ?>
                           </label>
                           <div class="input-group">
                              <input type="number" class="form-control"<?php if($value == 0){echo ' disabled'; } ?> name="cycles" id="cycles" value="<?php echo $value; ?>" <?php if(isset($task) && $task->total_cycles > 0){echo 'min="'.($task->total_cycles).'"';} ?>>
                              <div class="input-group-addon">
                                 <div class="checkbox">
                                    <input type="checkbox"<?php if($value == 0){echo ' checked';} ?> id="unlimited_cycles">
                                    <label for="unlimited_cycles"><?php echo _l('cycles_infinity'); ?></label>
                                 </div>
                              </div>
                           </div>
                        </div>
                      </div>
                    </div>
                    
                  </div>
              </div>
            </div>
          </div>

          <div class="btn-bottom-toolbar btn-toolbar-container-out text-right custom_width">
            <a class="btn btn-primary" href="<?php echo admin_url('payroll'); ?>">Back</a>
            <button class="btn btn-info only-save customer-form-submiter">
            <?php echo _l( 'submit'); ?>
            </button>
          </div>
        <?php echo form_close(); ?>
      </div>
   </div>
</div>

<div class="modal fade" id="add_comp_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <!-- Add New Company PopUp Start -->
         <h4 class="modal-title" id="myModalLabel">Add New Company</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
              <?php echo form_open(admin_url('payroll/addcompany')); ?>
              <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
              <div class="form-group">
                <label for="default_language" class="control-label">Company Type</label>
                <select name="customer_type" id="customer_type" class="form-control selectpicker" 
                 data-none-selected-text="<?php echo 'Select Company Type'; ?>">
                    <option value=""></option>
                    <?php foreach($client_type_data->result() as $client_type) { ?>
                       <option value="<?php echo $client_type->client_type?>"><?php echo $client_type->client_type?></option>
                    <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="name" class="control-label">Company Name</label>
                <input type="text" name="company" class="form-control" value="">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-info btn-sm">Save</button>
              </div>
              <?php echo form_close(); ?>
            </div>
            <div class="col-md-4">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php init_tail(); ?>

<script>
    $('#client').on('change', function() {
        if( $(this).find(":selected").val() == 'addnew')
        {
          $('#add_comp_modal').modal({
            show: true
          });
        }
    });
</script>

<script> 
    $(document).ready(function () {
      // Denotes total number of rows 
      var rowIdx = 0; 
      // jQuery button click event to add a row 
      $('#addBtn').on('click', function () { 
  
        // Adding a row inside the tbody. 
        $('#tbody').append(`<tr id="R${++rowIdx}"> 
            <td>
                <div class="form-group">
                   <select id="states${rowIdx}" name="states${rowIdx}" class="form-group" style="width:100%; padding:7px" class="selectpicker">
                    <option value="">Select State</option>
                    <option value="Alabama">Alabama</option>
                    <option value="Alaska">Alaska</option>
                    <option value="Arizona">Arizona</option>
                    <option value="Arkansas">Arkansas</option>
                    <option value="California">California</option>
                    <option value="Colorado">Colorado</option>
                    <option value="Connecticut">Connecticut</option>
                    <option value="Delaware">Delaware</option>
                    <option value="District Of Columbia">District Of Columbia</option>
                    <option value="Florida">Florida</option>
                    <option value="Georgia">Georgia</option>
                    <option value="Hawaii">Hawaii</option>
                    <option value="Idaho">Idaho</option>
                    <option value="Illinois">Illinois</option>
                    <option value="Indiana">Indiana</option>
                    <option value="Iowa">Iowa</option>
                    <option value="Kansas">Kansas</option>
                    <option value="Kentucky">Kentucky</option>
                    <option value="Louisiana">Louisiana</option>
                    <option value="Maine">Maine</option>
                    <option value="Maryland">Maryland</option>
                    <option value="Massachusetts">Massachusetts</option>
                    <option value="Michigan">Michigan</option>
                    <option value="Minnesota">Minnesota</option>
                    <option value="Mississippi">Mississippi</option>
                    <option value="Missouri">Missouri</option>
                    <option value="Montana">Montana</option>
                    <option value="Nebraska">Nebraska</option>
                    <option value="Nevada">Nevada</option>
                    <option value="New Hampshire">New Hampshire</option>
                    <option value="New Jersey">New Jersey</option>
                    <option value="New Mexico">New Mexico</option>
                    <option value="New York">New York</option>
                    <option value="North Carolina">North Carolina</option>
                    <option value="North Dakota">North Dakota</option>
                    <option value="Ohio">Ohio</option>
                    <option value="Oklahoma">Oklahoma</option>
                    <option value="Oregon">Oregon</option>
                    <option value="Pennsylvania">Pennsylvania</option>
                    <option value="Rhode Island">Rhode Island</option>
                    <option value="South Carolina">South Carolina</option>
                    <option value="South Dakota">South Dakota</option>
                    <option value="Tennessee">Tennessee</option>
                    <option value="Texas">Texas</option>
                    <option value="Utah">Utah</option>
                    <option value="Vermont">Vermont</option>
                    <option value="Virginia">Virginia</option>
                    <option value="Washington">Washington</option>
                    <option value="West Virginia">West Virginia</option>
                    <option value="Wisconsin">Wisconsin</option>
                    <option value="Wyoming">Wyoming</option>
                   </select>
                </div>
            </td>
            <td>
              <?php foreach($state_forms as $stf) { ?>
              <div class="col-md-12">
                <div class="col-md-4">
                  <div class="form-group">
                    <select name="type${rowIdx}[]" data-name="<?php echo $stf['id'].'_status' ?>${rowIdx}" class="form-group" style="width:100%; padding:7px">
                      <option value="">Nothing selected</option>
                      <?php foreach($state_forms as $st) { ?>
                      <option value="<?php echo $st['id'].'|'.$st['state_type'] ?>"><?php echo $st['state_type'] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-8" id="<?php echo $stf['id'].'_status' ?>${rowIdx}">
                    <div class="form-group">
                      <input style="padding-left:9px;" type="radio" name="<?php echo $stf['state_type'].'_status'; ?>${rowIdx}" value="1"><label for="type">&nbsp;Pending</label>
                    <input style="margin-left:9px;" type="radio" name="<?php echo $stf['state_type'].'_status'; ?>${rowIdx}" value="2"><label for="type">&nbsp;In Progress</label>
                    <input style="margin-left:9px;" type="radio" name="<?php echo $stf['state_type'].'_status'; ?>${rowIdx}" value="3"><label for="type">&nbsp;Filed</label>
                    </div>
                </div>
              </div>
              <?php }?>
            </td>

           <td class="text-center"> 
            <button class="btn btn-danger remove" type="button">Remove</button> 
           </td> 
          </tr>`); 
      }); 
  
      // jQuery button click event to remove a row. 
      $('#tbody').on('click', '.remove', function () { 
        var child = $(this).closest('tr').nextAll(); 
        child.each(function () { 
        var id = $(this).attr('id'); 
        // Getting the <p> inside the .row-index class. 
        var idx = $(this).children('.row-index').children('p'); 
        // Gets the row number from <tr> id. 
        var dig = parseInt(id.substring(1)); 
        // Modifying row index. 
        idx.html(`Row ${dig - 1}`); 
  
          // Modifying row id. 
          $(this).attr('id', `R${dig - 1}`); 
        }); 
  
          // Removing the current row. 
          $(this).closest('tr').remove(); 
          // Decreasing total number of rows by 1. 
          rowIdx--; 
      }); 

      function deletest() {
        var st=confirm("Are you sure to delete this state?")
        if (st) {
          return true;
        } else {
          return false;
        }
      }

    }); 
</script> 

<script>
function update() {
  var checkbox = $(this);
  var frm_status = checkbox.data('name');
  //alert(frm_status);
  if(checkbox.is(':checked')) {  
    $('#' + frm_status).show();
  }
  else {
    $('#' + frm_status).hide();
  }
}

$('.st_forms').change(update).each(update);
</script>

</body>
</html>
